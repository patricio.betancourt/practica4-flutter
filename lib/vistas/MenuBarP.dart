import 'package:auto/servicios/utiles/Utilidades.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MenuBarP extends StatelessWidget{
  const MenuBarP({super.key});
  @override
  Widget build(BuildContext context){
    return PopupMenuButton(itemBuilder: (context){
      return [
        PopupMenuItem<int>(
          value: 3,
          child: Text("Inicio"),
        ),
        PopupMenuItem<int>(
        value: 0,
          child: Text("Carrito"),
        ),
        PopupMenuItem<int>(
          value: 1,
          child: Text("Mi cuenta"),
        ),
        PopupMenuItem<int>(
          value: 2,
          child: Text("Cerrar sesion"),
        ),
      ];

    },
    onSelected: (value){
      if(value == 0){
        print("Menu de cuenta seleccionado");
        Navigator.pushNamed(context, '/carrito');
      }else if (value == 1){
        print("Ajustes de menu");
      }else if(value == 2){
        Utilidades util = Utilidades();
        util.removeAllValue();
        final SnackBar mensaje = SnackBar(content: Text('Bye'));
        ScaffoldMessenger.of(context).showSnackBar(mensaje);
        Navigator.pushNamed(context, '/home');
      } else if(value == 3) {
        Navigator.pushNamed(context, '/principal');
      }
    },
    );
  }

}