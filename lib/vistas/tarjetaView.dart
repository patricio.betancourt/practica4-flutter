import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../servicios/utiles/Utilidades.dart';

class tarjetaView extends StatefulWidget {
  const tarjetaView({
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _tarjetaViewState();
  }
}

class _tarjetaViewState extends State<tarjetaView> {
  Utilidades u = new Utilidades();
  late String? checkoutId;

  @override
  void initState() {
    super.initState();
    checkCheckoutId();
  }

  void checkCheckoutId() async {
    checkoutId = await u.getCheckoutId();
    setState(() {}); // Actualiza el estado para que se cargue el HTML
  }

  String getHtmlWithCheckoutId() {
    return """<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <style id="customPageStyle">
        body {
            background-color: #f6f6f5;
        }
    </style>
    <style>
        .centrar {
            margin-right: 150px;
            margin-left: 100px;
            border: 1px solid #808080;
            padding: 10px;
        }
    </style>
    <script>var wpwlOptions = {style:"plain"}</script>
    <script src="https://eu-test.oppwa.com/v1/paymentWidgets.js?checkoutId=$checkoutId"></script>
</head>

<body>
    <div class="centrar">
        <form action="https://flutter.dev" class="paymentWidgets" data-brands="VISA MASTER"></form>
    </div>
</body>

</html>
<html>""";
  }

  final Completer<WebViewController> _controller = Completer<WebViewController>();

  void cargarHtml() async {
    final url = Uri.dataFromString(
      getHtmlWithCheckoutId(),
      mimeType: 'text/html',
      encoding: Encoding.getByName('utf-8'),
    ).toString();

    final webViewController = await _controller.future;
    webViewController.loadUrl(url);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pasarela'),
      ),
      body: Stack(
        children: [
          WebView(
            initialUrl: Uri.dataFromString(
              getHtmlWithCheckoutId(),
              mimeType: 'text/html',
              encoding: Encoding.getByName('utf-8')!,
            ).toString(),
            javascriptMode: JavascriptMode.unrestricted,
          ),
          Positioned(
            bottom: 16, // Ajusta esta posición según tu diseño
            right: 16, // Ajusta esta posición según tu diseño
            child: FloatingActionButton(
              onPressed: () {
                u.eliminarCarrito();
                Navigator.pushNamed(context, '/carrito');
              },
              child: Icon(Icons.arrow_back),
            ),
          ),
        ],
      ),
    );
  }
}