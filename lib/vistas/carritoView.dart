import 'dart:convert';
import 'dart:developer';

import 'package:auto/servicios/facade/modelo/CarritoWS.dart';
import 'package:auto/servicios/utiles/Utilidades.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../servicios/conexion.dart';
import '../servicios/modelo/RespuestaGenerica.dart';
import 'MenuBarP.dart';

/// Flutter code sample for [DataTable].
class carritoView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _carreitoView();
  }
}

class _carreitoView extends State<carritoView> {
  //const carreitoView({super.key});
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  double subtotal = 0.0;
  double iva = 0.0;
  double total = 0.0;

  @override
  void initState() {
    super.initState();
    //_listar();
    _sumar();
    //_listar();
    //fetchCartData();
    //total = total;
    //subTotalCart = subTotalCart;
  }

  Future<List<CarritoWS>> _listar() async {
    double total = 0.0;
    List<CarritoWS> lista = [];
    Utilidades u = new Utilidades();
    String aux = 'carrito';
    String? datos = await u.getValue(aux);
    Map<dynamic, dynamic> mapa = jsonDecode(datos!);
    for (Map<dynamic, dynamic> v in mapa.values) {
      CarritoWS c = CarritoWS.fromMap(v);
      lista.add(c);
      total += c.cant * c.pu;
    }
    this.total = total;
    return lista;
  }

  void _sumar() async {
    await _listar();
    setState(() {
      subtotal = (total / 1.12);
      iva = total - subtotal;
    });
  }
  void _revisar() async {
    Utilidades u = new Utilidades();
    conexion conn = new conexion();
    String? id = await u.getCheckoutId();
    log('aca');
    String aux = "https://test.oppwa.com/v1/checkouts/"+ id! +"/payment";
    log(aux);
    RespuestaGenerica response = await conn.solicitudGet("pasarela/comprobar/"+id , "NO");
    log('para el codigo');
    log(response.code.toString());
    Map<String, dynamic> mapa = jsonDecode(response.info);
    Map<String, dynamic> info = mapa["info"];  // Accede directamente al mapa "info"
    if(response.code != 200){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error en la consulta'),),);
    }else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('La transacción se a realizao con éxito'),),);
    }
  }
  void _pagar() async{
    Map<dynamic, dynamic> data = {
      'amount': total,
    };

    Utilidades u = new Utilidades();
    conexion conn = new conexion();

    RespuestaGenerica response = await conn.solicitudPost("pasarela/checkout", data, "NO");
    Map<String, dynamic> mapa = jsonDecode(response.info);
    Map<String, dynamic> info = mapa["info"];  // Accede directamente al mapa "info"
    if(response.code != 200){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Pos no valio :v'),),);
    }else{
      var checkoutId = info["result"]["id"];
      //log(checkoutId.toString());
      u.saveCheckoutId(checkoutId);
      Navigator.pushNamed(context, '/tarjeta');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Carrito de compras"),
        elevation: 10,
        automaticallyImplyLeading: false,
        actions: const [MenuBarP()],
      ),
      body: Column(
        children: [
          ClipOval(
            child: Image(
              image: NetworkImage(
                  'https://images.vexels.com/media/users/3/200097/isolated/preview/942820836246f08c2d6be20a45a84139-icono-de-carrito-de-compras-carrito-de-compras.png'),
              alignment: Alignment.center,
              width: 100,
              height: 100,
            ),
          ),
          Container(
            child: const Text(
              "Carrito de compras",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontStyle: FontStyle.italic,
                  color: Colors.blue,
                  fontWeight: FontWeight.w500,
                  fontSize: 30),
            ),
            padding: const EdgeInsets.all(20),
          ),
          Flexible(
            child: FutureBuilder(
              key: refreshKey,
              future: _listar(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                      child:
                      Text('El carrito esta cargado o no tiene productos'));
                  //return Center(child: CircularProgressIndicator());
                }
                if (snapshot.hasData) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    verticalDirection: VerticalDirection.down,
                    children: <Widget>[
                      Flexible(
                        child: Container(
                            padding: EdgeInsets.all(5),
                            child: dataBody(snapshot.data)),
                      )
                    ],
                  );
                }

                return Center();
              },
            ),
          ),
          Flexible(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: [
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.all(5),
                        child: dataFooter(subtotal, iva, total)),
                  )
                ],
              )),
          Container(
            height: 50,
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.deepOrange[300]!),
                  ),
                  child: const Text('Borrar'),
                  onPressed: () {
                    Utilidades u = new Utilidades();
                    u.eliminarCarrito();
                    Navigator.pushNamed(context, '/carrito');
                  },
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.teal[300]!),
                  ),
                  child: const Text('Revisar'),
                  onPressed: _revisar,
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.lightBlue.shade300!),
                  ),
                  child: const Text('Pagar'),
                  onPressed: _pagar,
                ),

              ],
            ),
          ),


        ],
      ),
    );
  }
}

SingleChildScrollView dataBody(List<CarritoWS>? listSales) {
  return SingleChildScrollView(
    scrollDirection: Axis.vertical,
    child: DataTable(
      headingRowColor: MaterialStateColor.resolveWith(
            (states) {
          return Colors.blue.shade400;
        },
      ),
      columnSpacing: 12,
      dividerThickness: 5,
      dataRowMaxHeight: 80,
      dataTextStyle:
      const TextStyle(fontStyle: FontStyle.normal, color: Colors.black),
      headingRowHeight: 50,
      headingTextStyle:
      const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      horizontalMargin: 10,
      showBottomBorder: true,
      showCheckboxColumn: false,
      columns: [
        DataColumn(
            label: Expanded(child: Center(child: Text("cant"))),
            numeric: true,
            tooltip: "Cantidad"),
        DataColumn(
          label: Expanded(child: Center(child: Text("Detalle"))),
          numeric: false,
          tooltip: "Detalle",
        ),
        DataColumn(
          label: Expanded(child: Center(child: Text("PU"))),
          numeric: true,
          tooltip: "Precio unitario",
        ),
        DataColumn(
          label: Expanded(child: Center(child: Text("PT"))),
          numeric: true,
          tooltip: "Precio total",
        ),
      ],
      rows: listSales!
          .map(
            (sale) => DataRow(
            onSelectChanged: (b) {
              log(sale.toString());
            },
            cells: [
              DataCell(
                  Text(sale.cant.toString(), textAlign: TextAlign.center)),
              DataCell(
                Text(sale.desc, textAlign: TextAlign.center),
              ),
              DataCell(
                Text("\$" + sale.pu.toString(),
                    textAlign: TextAlign.center),
              ),
              DataCell(
                Text("\$" + sale.pt.toString(),
                    textAlign: TextAlign.center),
              ),
            ]),
      )
          .toList(),
    ),
  );
}

GridView dataFooter(double subtotal, double iva, double total) {
  return GridView.count(
    crossAxisCount: 2,

    mainAxisSpacing: 10.0,
    crossAxisSpacing: 10.0,
    childAspectRatio: 4.0,

    children: [
      Container(child: Text('SUBTOTAL', style: TextStyle(
          fontStyle: FontStyle.italic,
          color: Colors.black,
          fontWeight: FontWeight.w500,
          fontSize: 15)),alignment: Alignment.centerRight),
      Container(child: Text("\$" +subtotal.toStringAsFixed(2)),alignment: Alignment.centerRight),
      Container(child: Text('IVA', style: TextStyle(
          fontStyle: FontStyle.italic,
          color: Colors.black,
          fontWeight: FontWeight.w500,
          fontSize: 15)),alignment: Alignment.centerRight),
      Container(child: Text("\$" +iva.toStringAsFixed(2)),alignment: Alignment.centerRight),
      Container(child: Text('TOTAL', style: TextStyle(
          fontStyle: FontStyle.italic,
          color: Colors.black,
          fontWeight: FontWeight.w500,
          fontSize: 15)),alignment: Alignment.centerRight),
      Container(child: Text("\$" +total.toStringAsFixed(2)),alignment: Alignment.centerRight),
    ],
  );
}