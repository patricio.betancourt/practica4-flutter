import 'dart:developer';

import 'package:auto/servicios/conexion.dart';
import 'package:auto/servicios/facade/InicioSesionFacade.dart';
import 'package:auto/servicios/facade/modelo/InicioSesionWS.dart';
import 'package:auto/servicios/utiles/Utilidades.dart';
import 'package:auto/vistas/menuView.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';

class registroView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _registroViewState();
  }
}

class _registroViewState extends State<registroView> {
  final _formKey = GlobalKey<FormState>();
  String tipo_ident = "";
  //InicioSesionWS sesionWS = new InicioSesionWS();
  final TextEditingController correoControl = TextEditingController();
  final TextEditingController claveControl = TextEditingController();

  void _iniciar() {
    setState(() async {
      if (_formKey.currentState!.validate()) {
        InicioSesionFacade sesion = new InicioSesionFacade();
        //conexion conn = new conexion();
        Map<dynamic, dynamic> mapa = {
          "email": correoControl.text,
          "clave": claveControl.text
        };
        sesion.inicioSesion(mapa).then((value) {
          if (value.token != "") {
            log(value.correo);
            Utilidades util = new Utilidades();
            util.saveValue("token", value.token);
            final SnackBar mensaje =
                SnackBar(content: Text('Binevenido!! ' + value.user));
            ScaffoldMessenger.of(context).showSnackBar(mensaje);
            Navigator.pushNamed(context, '/principal',
                arguments: {"mensaje": "Hola"});
          } else {
            final SnackBar mensaje = SnackBar(content: Text(value.msg));
            ScaffoldMessenger.of(context).showSnackBar(mensaje);
          }
        }).catchError((error) {
          log("Hay un error");
          log(error.toString());
        });
        //sesionWS = await sesion.inicioSesion(mapa);
        //log(sesionWS.msg);
        //conn.solicitudPost("sesion", mapa, "NO");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Scaffold(
            body: ListView(
          padding: const EdgeInsets.all(32),
          children: <Widget>[
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  "Aplicacion de mí :v",
                  style: TextStyle(
                      color: Colors.teal,
                      fontWeight: FontWeight.w500,
                      fontSize: 30),
                )),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Registrar usuario',
                  style: TextStyle(fontSize: 20),
                )),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextFormField(
                obscureText: true,
                controller: claveControl,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Nombre',
                  suffixIcon: Icon(Icons.account_box),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Nombre requerido';
                  }
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextFormField(
                obscureText: true,
                controller: claveControl,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Apellido',
                  suffixIcon: Icon(Icons.account_box),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Apellido requerido';
                  }
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: correoControl,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Correo',
                  suffixIcon: Icon(Icons.alternate_email),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Correo requerido';
                  }
                  if (!isEmail(value)) {
                    return 'Debe ser un correo valido';
                  }
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: DropdownButtonFormField(
                items: dropdownItems,
                decoration: InputDecoration(
                  labelText: 'Tipo de identificación',
                  border: InputBorder.none,
                  suffixIcon: Icon(Icons.type_specimen),
                ),
                validator: (value) {
                  if (value == null) {
                    return 'Seleccion un tipo de identificación';
                  }
                },
                onChanged: (String? value) {
                  if (value != null) {
                    setState(() {
                      this.tipo_ident = value;
                    });
                  }
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: correoControl,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Identificación',
                  suffixIcon: Icon(Icons.alternate_email),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Identificacion requerido';
                  }
                  if (!isEmail(value)) {
                    return 'Debe ser un correo valido';
                  }
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextFormField(
                obscureText: true,
                controller: claveControl,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Clave',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Clave requerida';
                  }
                },
              ),
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: ElevatedButton(
                  child: const Text('Registrar'),
                  onPressed: _iniciar,
                  //print(nameController.text);
                  //print(passwordController.text
                )),
          ],
        )));
  }

  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("CEDULA"), value: "CEDULA"),
      DropdownMenuItem(child: Text("PASAPORTE"), value: "PASAPORTE"),
      DropdownMenuItem(child: Text("RUC"), value: "RUC"),
    ];
    return menuItems;
  }
}
