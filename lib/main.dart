import 'package:auto/vistas/exception/Page404.dart';
import 'package:auto/vistas/menuView.dart';
import 'package:auto/vistas/registroView.dart';
import 'package:auto/vistas/sessionView.dart';
import 'package:auto/vistas/carritoView.dart';
import 'package:auto/vistas/tarjetaView.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter ',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blueGrey),
        useMaterial3: true,
      ),
      debugShowCheckedModeBanner: false,
      home: sessionView(),
      initialRoute: '/home',
      routes:{
        '/home': (context) => sessionView(),
        '/principal': (context)=> menuView(),
        '/registrar': (context)=> registroView(),
        '/carrito': (context)=> carritoView(),
        '/tarjeta': (context)=> tarjetaView(),
      },
      onGenerateRoute: (settings){
        return MaterialPageRoute(builder: (context)=> Page404());
      },
    );
  }
}

