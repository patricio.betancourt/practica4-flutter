import 'dart:convert';
import 'dart:developer';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../facade/modelo/AutosWS.dart';

class Utilidades {
  final _storage = const FlutterSecureStorage();

  Future<String?> getValue(valor) async {
    String? aux = await _storage.read(key: valor);
    log(aux.toString());
    return aux;
  }

  void saveValue(llave, valor) async {
    await _storage.write(key: llave, value: valor);
  }

  void removeAllValue() async {
    await _storage.deleteAll();
  }

  void eliminarCarrito() async{
    await _storage.delete(key: 'carrito');
  }

  void addCarrito(valor) async{
    log('aniadir Carrito');
    var carrito = await _storage.read(key: 'carrito');
    if(carrito != null){
      Map<dynamic, dynamic> carro = jsonDecode(carrito.toString());
      var data = carro[valor.external_id];
      if(data != null){
        data['cant'] = data['cant']+1;
        carro[valor.external_id] = data;
        await _storage.write(key: 'carrito', value: jsonEncode(carro));
      }else{
        carro[valor.external_id] = {"valor":AutosWS.serialize(valor), "cant":1};
        await _storage.write(key: 'carrito', value: jsonEncode(carro));
      }
    }else{
      Map<dynamic, dynamic> carro = {};
      carro[valor.external_id] = {"valor":AutosWS.serialize(valor), "cant":1};
      await _storage.write(key: 'carrito', value: jsonEncode(carro));
    }
    log('Sirve por fin');
  }

  void saveCheckoutId(String checkoutId) async {
    await _storage.write(key: "id_pagar", value: checkoutId);
  }

  Future<String?> getCheckoutId() async {
    log(_storage.read(key: "id_pagar").toString());
    return await _storage.read(key: "id_pagar");
  }

  void deleteCheckoutId() async{
    await _storage.delete(key: "id_pagar");
  }
}
