import 'dart:convert';
import 'dart:developer';

class CarritoWS{
  int cant = 0;
  double pu = 0.0;
  double pt = 0.0;
  String desc = "";
  String envio = "";
  CarritoWS();
  CarritoWS.fromMap(Map<dynamic, dynamic> mapa) {
    log('CARRITOWS');
    log(mapa.length.toString());
    if(mapa.isNotEmpty) {
      Map<dynamic, dynamic> map = jsonDecode(mapa['valor']);
      cant = int.parse(mapa['cant'].toString()) ;
      desc = 'Vehiculo: '+map['nombre_marca']+' '+map['nombre']+' '+map['anio'].toString()+' placa: '+map['placa'];
      pu = double.parse(map['precio'].toString());
      pt = pu * cant;
      envio = map['external_id']+':'+cant.toString();
    }
    log('fin carritows');
  }
}