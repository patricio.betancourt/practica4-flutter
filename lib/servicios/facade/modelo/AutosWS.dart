
import 'dart:convert';

class AutosWS {
  String nombre = '';
  String external_id='';
  String placa = '';
  String duenio = '';
  double precio = 0.0;
  String foto = '';
  int anio = 0;
  bool estado_vendido = false;
  String nombre_marca = '';
  AutosWS();

  AutosWS.fromMap(Map<dynamic, dynamic> mapa){
    nombre = mapa['modelo'];
    external_id = mapa['external_id'];
    foto = mapa["imagen"];
    placa = mapa["placa"];
    precio = double.parse(mapa["precio"].toString());
    anio = int.parse(mapa["anio"].toString());
    nombre_marca = mapa['marca']["nombre"];
  }

  static Map<String, dynamic> toMap(AutosWS model)=>
      <String, dynamic>{
    "nombre": model.nombre,
        "external_id": model.external_id,
        "precio": model.precio,
        "anio": model.anio,
        "nombre_marca": model.nombre_marca,
        "estado_vendido": model.estado_vendido,
        "placa": model.placa
      };
  static String serialize(AutosWS model)=>
      json.encode(AutosWS.toMap(model));
}
